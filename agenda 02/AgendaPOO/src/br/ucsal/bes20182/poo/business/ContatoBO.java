package br.ucsal.bes20182.poo.business;

import java.util.List;

import br.ucsal.bes20182.poo.domain.Contato;
import br.ucsal.bes20182.poo.persistence.ContatoDAO;

public class ContatoBO {
	
	public ContatoDAO contatoDAO = new ContatoDAO();
	
	public String incluir(Contato contato) {
		String erro = validar(contato);
		if (erro != null) {
			return erro;
		}
		return contatoDAO.incluir(contato);
	}
	
	public List<Contato> obterTodos() {
		return contatoDAO.ObterTodos();
	}
	
	private String validar (Contato contato) {
		if (contato.getNome().trim().isEmpty()) {
			return "Nome n�o informado!";
		}
		
		if (contato.getTelefone().trim().isEmpty()) {
			return "Telefone n�o informado!";
		}
		
		return null;
	}

}
