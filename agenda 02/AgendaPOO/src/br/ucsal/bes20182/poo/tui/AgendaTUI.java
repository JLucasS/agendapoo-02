package br.ucsal.bes20182.poo.tui;

import java.util.List;
import java.util.Scanner;

import br.ucsal.bes20182.poo.business.ContatoBO;
import br.ucsal.bes20182.poo.domain.Contato;

public class AgendaTUI {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		
		Integer cont;
		System.out.println("Informe a quantidade de contatos desejada");
		cont = sc.nextInt();
		sc.nextLine();
		
		for (int i = 0; i < cont; i++) {
			AgendaTUI.Incluir();
			AgendaTUI.Listar();
		}


	}

	public static ContatoBO contatoBO = new ContatoBO();

	public static void Incluir() {

		String nome = obterTexto("Informe o nome: ");

		String telefone = obterTexto("Informe o telefone: ");

		Integer anoNasc = obterNum("Informe o ano de nascimento: ");

		Contato contato = new Contato(nome, telefone, anoNasc);

		String erro = contatoBO.incluir(contato);
		if (erro != null) {
			System.out.println("Opera��o Falhou");
			System.out.println(erro);
		}

	}

	public static void Listar() {
		List<Contato> contatos = contatoBO.obterTodos();

		for (int i = 0; i < contatos.size(); i++) {
			System.out.println(" Contato " + (i + 1));
			System.out.println(" \t" + contatos.get(i).getNome());
			System.out.println(" \tTelefone: " + contatos.get(i).getTelefone());
			System.out.println(" \tAno de nascimento: " + contatos.get(i).getAnoNasc());
		}
	}

	private static String obterTexto(String mensagem) {

		System.out.println(mensagem);
		return sc.nextLine();
	}

	private static Integer obterNum(String mensagem) {
		System.out.println(mensagem);
		Integer numero = sc.nextInt();
		sc.nextLine();
		return numero;
	}

}
