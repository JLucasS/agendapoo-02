package br.ucsal.bes20182.poo.domain;

public class Contato {
	
	private String nome;
	
	private String telefone;
	
	private Integer anoNasc;

	
	public Contato(String nome, String telefone, Integer anoNasc ) {
		this.nome = nome;
		this.telefone = telefone;
		this.anoNasc = anoNasc;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Integer getAnoNasc() {
		return anoNasc;
	}

	public void setAnoNasc(Integer anoNasc) {
		this.anoNasc = anoNasc;
	}

}
